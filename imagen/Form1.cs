﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace imagen
{
    public partial class Form1 : Form
    {
        List<Imagen> li;
        Graphics gr;
        bool direccion;
        public Form1()
        {
            li = new List<Imagen>();
            direccion = true;
            InitializeComponent();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            gr = this.CreateGraphics();
            Image newImage = Image.FromFile("emoticonos-26.png");
            Imagen i = new Imagen(newImage, e.X, e.Y,direccion);
            li.Add(i);
            gr.DrawImage(newImage, e.X, e.Y);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
             this.Refresh();
            gr = this.CreateGraphics();
            foreach (Imagen i in li)
            {
                if (i.x + 50 >= this.Width)
                {
                    //i.x = 5;
                    i.dir = false;
                }
                if (i.x <= 0)
                    i.dir = true;
                if (i.dir)
                    i.x += 5;
                else
                    i.x -= 5;
                gr.DrawImage(i.img, i.x, i.y);
            }
        }


    }

    
}
