﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace imagen
{
    class Imagen
    {
        public Image img;
        public int x;
        public int y;
        public bool dir;

       public Imagen(Image i, int x, int y,bool dir)
        { 
            this.img=i;
            this.x = x;
            this.y = y;
            this.dir = dir;
        }
    }
}
